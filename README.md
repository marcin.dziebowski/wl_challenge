# Matchmaker

Matchmaker is an application for managing candidate profiles.

## Run local env with Docker

1. Download, install and configure Docker in your system.
2. Clone project from repository.
3. run command `docker-compose up`
4. Run `composer install`
5. Address in the browser (http://localhost:8999/) will return results
