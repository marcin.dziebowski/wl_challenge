<?php

use Interview\Command\ProvidePageContentDataCommand;
use Interview\Services\IoC;

require_once 'vendor/autoload.php';
require_once 'src/Services/boot.php';

$pageContentData = IoC::get(ProvidePageContentDataCommand::class);
echo $pageContentData->getContent();