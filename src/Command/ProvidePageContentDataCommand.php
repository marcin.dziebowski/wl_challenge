<?php

namespace Interview\Command;

use Interview\Services\PageContentService;
use Interview\Services\PageContentHandlerInterface;

class ProvidePageContentDataCommand
{
    /** @var string  */
    const WEB_URL = 'https://wltest.dns-systems.net/';

    /** @var PageContentService  */
    private PageContentService $pageContentService;

    public function __construct(PageContentHandlerInterface $pageContentHandler)
    {
        $this->pageContentService = $pageContentHandler;
    }

    public function getContent(): string
    {
        return json_encode($this->pageContentService->handle(self::WEB_URL), JSON_UNESCAPED_UNICODE );
    }
}