<?php

namespace Interview\Services;

interface PageContentHandlerInterface
{
    /**
     * @param string $url
     * @return array
     */
    public function handle(string $url): array;
}