<?php

namespace Interview\Services;

use DOMDocument;
use DOMXPath;

class PageContentService implements PageContentHandlerInterface
{
    /** @var DOMXPath */
    private DOMXPath $finder;

    /**
     * @inheritDoc
     */
    public function handle(string $url): array
    {
        $content = file_get_contents($url);
        //create DOMDocument object and parse content from url
        $dom = new DOMDocument;
        @$dom->loadHTML($content);
        $this->finder = new DomXPath($dom);

        $elements = $this->prepareElementsArray($dom);
        $results = $this->prepareResultsArray($elements);

        //sort array by annual price with the most expensive package first.
        usort($results, function ($item1, $item2) {
            return $this->extractPriceFromString($item2['price']) <=> $this->extractPriceFromString($item1['price']); //call items in text method to extract float number price for sorting
        });

        return $results;
    }

    //prepare data elements array by DOMDocument object
    public function prepareElementsArray(DOMDocument $dom): array
    {
        return [
            'count_of_elements' => $this->finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' package ')]")->count(), //get count of all elements with class package
            'titles' => $dom->getElementsByTagName('h3'), // get all titles with h3
            'prices' => $this->finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' price-big ')]"), // get all titles with h3
            'descriptions' =>  $this->finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' package-description ')]"), // get all element with class package-price
            'discounts' =>  $this->finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' package-description ')]"), // get all element with class package-price
        ];
    }

    //prepare final array results array with required data
    public function prepareResultsArray(array $elements): array
    {
        $results = [];
        //foreach elements by count of all and assign proper keys and values
        for ($i = 0; $i < $elements['count_of_elements']; $i++) {
            $results[$i]['option_title'] = $elements['titles'][$i]->nodeValue;
            $results[$i]['description'] = $elements['descriptions'][$i]->nodeValue;
            $results[$i]['price'] = $elements['prices'][$i]->nodeValue;
            $results[$i]['discount'] = $this->extractDiscountFromDOMElement($elements['discounts'], $i);
        }

        return $results;
    }

    //extract discount information from string sentence from DOMDocument
    public function extractDiscountFromDOMElement(\DOMNodeList $discounts, int $key): float
    {
        return $discounts[$key]->getElementsByTagName('p')->length > 0 ? $this->extractPriceFromString($discounts[$key]->getElementsByTagName('p')->item(0)->nodeValue) : 0;
    }

    //price from DOMDocument is string value with currency. This function extract float text from string and return it as float type
    public function extractPriceFromString(string $text): float
    {
        return (float) filter_var( $text, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
    }
}