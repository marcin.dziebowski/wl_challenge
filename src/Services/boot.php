<?php

use Interview\Services\IoC;
use Interview\Services\PageContentService;
use Interview\Services\PageContentHandlerInterface;

IoC::set(PageContentHandlerInterface::class, PageContentService::class);
