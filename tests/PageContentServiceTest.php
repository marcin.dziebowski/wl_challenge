<?php

use Interview\Command\ProvidePageContentDataCommand;
use Interview\Services\PageContentService;
use PHPUnit\Framework\TestCase;

class PageContentServiceTest extends TestCase
{
    private $pageContentServiceMock;

    private $domMock;

    protected function setUp(): void
    {
        parent::setUp();
        $this->pageContentServiceMock = $this->createMock(PageContentService::class);
        $this->domMock = $this->createMock(DOMDocument::class);
    }

    public function testWillReturnArray()
    {
        $mock = $this->createMock(PageContentService::class);
        $this->assertIsArray($mock->handle(ProvidePageContentDataCommand::WEB_URL));
    }

    public function testPrepareElementsArrayWillReturnArrayWithSpecificKeys()
    {
        $this->pageContentServiceMock->expects($this->any())
            ->method('prepareElementsArray')
            ->willReturn(['count_of_elements' => 'example', 'titles' => 'example', 'prices' => 'example', 'descriptions' => 'example', 'discounts' => 'example']);

        $arrayResult = $this->pageContentServiceMock->prepareElementsArray($this->domMock);
        $requiredKeys = ['count_of_elements', 'titles', 'prices', 'descriptions', 'discounts'];
        foreach ($requiredKeys as $requiredKey) {
            $this->assertArrayHasKey($requiredKey, $this->pageContentServiceMock->prepareElementsArray($this->domMock));
        }

        $this->assertIsArray($arrayResult);
    }

    public function testPrepareResultsArrayWillReturnArrayWithCountEqualProvidedElements()
    {
        $arrayResult = $this->pageContentServiceMock->prepareElementsArray($this->domMock);

        $this->pageContentServiceMock->expects($this->once())
            ->method('prepareResultsArray')
            ->willReturn(['count_of_elements' => 'example', 'titles' => 'example', 'prices' => 'example', 'descriptions' => 'example', 'discounts' => 'example']);

        $finalResults = $this->pageContentServiceMock->prepareResultsArray($arrayResult);
        $this->assertIsArray($finalResults);
    }

    public function testExtractDiscountFromDOMElementWillReturnNumber()
    {
        $floatNumber = 10.99;
        $this->pageContentServiceMock
            ->expects($this->once())
            ->method('extractDiscountFromDOMElement')
            ->willReturn($floatNumber);

        $this->pageContentServiceMock->extractDiscountFromDOMElement($this->createMock(DOMNodeList::class), 0);
    }

    public function testExtractPriceFromStringWillReturnNumber()
    {
        $exampleString = '10.99';
        $result = $this->pageContentServiceMock->extractPriceFromString($exampleString);

        $this->assertIsFloat($result);
    }
}