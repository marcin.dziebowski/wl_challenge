<?php

use Interview\Command\ProvidePageContentDataCommand;
use PHPUnit\Framework\TestCase;

class ProvidePageContentDataCommandTest extends TestCase
{
    public function testIsSiteAvailable()
    {
        $status = false;

        $headers = @get_headers(ProvidePageContentDataCommand::WEB_URL);
        if($headers && strpos( $headers[0], '200')) {
            $status = true;
        }

        $this->assertTrue($status);

        $data = file_get_contents(ProvidePageContentDataCommand::WEB_URL);
        $this->assertIsString($data);
    }
}